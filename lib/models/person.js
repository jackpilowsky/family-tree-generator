const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const person = new Schema({
  firstName: String,
  lastName: String,
  yearBorn: Number,
  gender: String
});

module.exports = person;