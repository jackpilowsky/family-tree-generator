var ApiMessages = function () { };
ApiMessages.EMAIL_NOT_FOUND = 'Email address not found';
ApiMessages.INVALID_PWD = 'Password does not match the one on our records';
ApiMessages.DB_ERROR = 'A database error has occurred. Please try again';
ApiMessages.NOT_FOUND = 'The record you were looking for was not found';
ApiMessages.EMAIL_ALREADY_EXISTS = 'This email address already exists';
ApiMessages.COULD_NOT_CREATE_USER = 'User could not be created';
ApiMessages.PASSWORD_RESET_EXPIRED = 'Password reset has expired';
ApiMessages.PASSWORD_RESET_HASH_MISMATCH = 'Something went wrong';
ApiMessages.PASSWORD_RESET_EMAIL_MISMATCH = 'Email does not match any on our records';
ApiMessages.COULD_NOT_RESET_PASSWORD = 'Password could not be reset';
ApiMessages.UNAUTHORIZED_ACTION = 'You are not allowed to do that';
ApiMessages.PROJECT_ALREADY_EXISTS = 'This project name is taken';
ApiMessages.USER_MUST_BE_Logged_IN = 'The user must be logged in';

module.exports = ApiMessages;