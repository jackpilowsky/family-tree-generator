
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const person = require('./person')


const partnershipSchema = new Schema({
  parent1: person,
  parent2: person,
  children: [person]
});

module.exports = partnershipSchema;