
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const parternship = require('./partnerships')
const user = require('./users')

const projectSchema = new Schema({
  owner: String,
  name: String,
  slug: String, 
  parternships: [parternship], // the top level partnerships
  editors: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}]
});

module.exports = projectSchema