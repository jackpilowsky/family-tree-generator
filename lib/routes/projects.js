const express = require('express');
const router = express.Router();
const ProjectController = require('./../controllers/projects.js');
const bodyParser = require('body-parser');

module.exports = function(session, app){
  const self = this;
  self.projectController = new ProjectController(session);
  router.post("/api/create-project", (req, res) => {
    console.log(projectController);
    const name =  req.body.name || '';
    if(!name){
      res.json({
        success: false,
          extras: {
            msg: "Name is required"
          } 
      })
    }
    self.projectController.createProject(name, function(err, apiResponse){
      if(err){
        console.log(err);
      }
      res.json(apiResponse);
      return;
    })
  })
  return router;
}