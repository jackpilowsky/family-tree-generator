const express = require('express');
const router = express.Router();
const uuid = require('node-uuid');
const AccountController = require('./../controllers/account.js');
const UserModel = require('./../models/users.js');
//set up body partser
const bodyParser = require('body-parser');


module.exports = function(session, app){
  const self = this;
  self.accountController = new AccountController(UserModel, session);
  
  router.post('/api/login', function(req, res){
    const email = req.body.email;
    const password = req.body.password;
    if(!email || !password){
      res.json({
        success: false,
        extras: {
          msg: "Email or password not provided"
        } 
      })
      return;
    }
    self.accountController.logon(email, password, function(err, apiResponse){
      if(err){
        console.log(err);
      }
      res.json(apiResponse);
      return;
    })
  });
 
  router.post('/api/signup', (req, res) => {
    const passwordSalt = uuid.v4();
    const password = req.body.password;
    self.accountController.hashPassword(password, passwordSalt, function(err, passwordHash){
      const newUser = new UserModel({
        email: req.body.email,
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        passwordHash: passwordHash,
        passwordSalt: passwordSalt
      });
      self.accountController.register(newUser, function(err, apiResponse){
        if(err){
          console.log(error);
        }
        res.json(apiResponse);
        return;
      })
    });
  })


  /* Handle Logout */
  router.get('/api/signout', function(req, res) {
    self.accountController.logoff();
    res.json({
      success: true,
      extras: {
        msg: ''
      }
    })
  });
  
  router.get('/api/fetchuser',(req, res) => {
    const userProfile = self.accountController.getUserProfile()
    res.json({
      success: Boolean(userProfile),
      extras: {
        userProfile: userProfile
      }
    })
  });


  return router;
}

