const Utils = require('../utils/Utils.js')
const ApiResponse = require('../models/api-response.js');
const ApiMessages = require('../models/api-messages.js');
const Project = require('../models/projects.js');
const Person = require('../models/person.js');
const Partnership = require('../models/partnerships.js');

class projectsController{
  constructor(session){
    this.session = session;
  }
  createProject(name, callback){
    if(!this.session.userProfile){
      return callback(null, new ApiResponse({ success: false, extras: { msg: ApiMessages.USER_MUST_BE_Logged_IN } }));
    }
    // create the first person
    const person = new Person({
      firstName: this.session.userProfile.firstName,
      lastName: this.session.userProfile.lastName,
    })
    // create the first partnership
    const partnership = new Partnership({
      children: [person]
    })

    // create the project
    const slug = name.replace(/[^\w-]/g, '-').toLowerCase();
    Project.findOne({ slug: slug }, function (err, project) {
      if (err) {
        return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.DB_ERROR } }));
      }
      if(project){
        return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.PROJECT_ALREADY_EXISTS } })); 
      }else{
        var project = new Project({
          owner: userProfile,
          name: name,
          slug: slug,
          partnership: partnership
        });
        project.save(function (err) {
          if(err){
            return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.DB_ERROR } }));
          }else{
            callback(err, new ApiResponse({
              success: true, extras: {
                project:project
              }
            }));
          }
        })
      }
    })
  }
  updateProject(partnerships, callback){

  }
  changeOwnership(oldOwner, newOwner, callback){
    if(this.session.userProfile != oldOwner){
      callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.UNAUTHORIZED_ACTION } }));
    }
    
  }
}
module.exports = projectsController;