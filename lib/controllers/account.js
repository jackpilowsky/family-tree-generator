const crypto = require('crypto');
const uuid = require('node-uuid');
const ApiResponse = require('../models/api-response.js');
const ApiMessages = require('../models/api-messages.js');
const UserProfileModel = require('../models/user-profile.js');

class AccountController{
  constructor(userModel, session, mailer){
    this.userModel = userModel;
    this.session = session;
    this.mailer = mailer;
  }
  getSession() {
    return this.session;
  }
  setSession(session) {
    this.session = session;
  }
  getUserProfile(){
    return this.session.userProfileModel;
  }
  hashPassword(password, salt, callback) {        
    // we use pbkdf2 to hash and iterate 10k times by default 
    const iterations = 10000;
    const keyLen = 64; // 64 bit
    const digest = 'sha512';
    crypto.pbkdf2(password, salt, iterations, keyLen, digest, callback);
  }
  logon(email, password, callback) {
    const self = this;

    self.userModel.findOne({ email: email }, function (err, user) {
      if (err) {
        callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.DB_ERROR } }));
      }
      if (user) {
        self.hashPassword(password, user.passwordSalt, function (err, passwordHash) {
          if (passwordHash == user.passwordHash) {
            var userProfileModel = new UserProfileModel({
              email: user.email,
              firstName: user.firstName,
              lastName: user.lastName
            });
            self.session.userProfileModel = userProfileModel;
            callback(err, new ApiResponse({
              success: true, extras: {
                userProfileModel:userProfileModel
              }
            }));
          } else {
            callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.INVALID_PWD } }));
          }
        });
      } else {
        callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.EMAIL_NOT_FOUND } }));
      }
    }); 
    return; 
  }
  logoff() {
    if (this.session.userProfileModel) delete this.session.userProfileModel;
    return;
  }
  register(newUser, callback) {
    var self = this;
    self.userModel.findOne({ email: newUser.email }, function (err, user) {
      if (err) {
        return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.DB_ERROR } }));
      }
      if (user) {
        return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.EMAIL_ALREADY_EXISTS } }));
      } else {
        newUser.save(function (err, user, numberAffected) {
          if (err) {
            return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.DB_ERROR } }));
          }
          if (numberAffected === 1) {

            var userProfileModel = new UserProfileModel({
                email: user.email,
                firstName: user.firstName,
                lastName: user.lastName
            });
            return callback(err, new ApiResponse({
              success: true, extras: {
                userProfileModel: userProfileModel
              }
            }));
          } else {
            return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.COULD_NOT_CREATE_USER } }));
          }             

        });
      }
    });
  }
  resetPassword(email, callback) {
    var self = this;
    self.userModel.findOne({ email: email }, function (err, user) {
      if (err) {
          return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.DB_ERROR } }));
      }
      // Save the user's email and a password reset hash in session. We will use
      var passwordResetHash = uuid.v4();
      self.session.passwordResetHash = passwordResetHash;
      self.session.emailWhoRequestedPasswordReset = email;

      self.mailer.sendPasswordResetHash(email, passwordResetHash);

      return callback(err, new ApiResponse({ success: true, extras: { passwordResetHash: passwordResetHash } }));
    })
  }
  resetPasswordFinal(email, newPassword, passwordResetHash, callback) {
    var self = this;
    if (!self.session || !self.session.passwordResetHash) {
      return callback(null, new ApiResponse({ success: false, extras: { msg: ApiMessages.PASSWORD_RESET_EXPIRED } }));
    }
    if (self.session.passwordResetHash !== passwordResetHash) {
      return callback(null, new ApiResponse({ success: false, extras: { msg: ApiMessages.PASSWORD_RESET_HASH_MISMATCH } }));
    }
    if (self.session.emailWhoRequestedPasswordReset !== email) {
      return callback(null, new ApiResponse({ success: false, extras: { msg: ApiMessages.PASSWORD_RESET_EMAIL_MISMATCH } }));
    }
    var passwordSalt = this.uuid.v4();
    self.hashPassword(newPassword, passwordSalt, function (err, passwordHash) {
      self.userModel.update({ email: email }, { passwordHash: passwordHash, passwordSalt: passwordSalt }, function (err, numberAffected, raw) {
        if (err) {
          return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.DB_ERROR } }));
        }
        if (numberAffected < 1) {
          return callback(err, new ApiResponse({ success: false, extras: { msg: ApiMessages.COULD_NOT_RESET_PASSWORD } }));
        } else {
          return callback(err, new ApiResponse({ success: true, extras: null }));
        }                
      });
    });
};
}


module.exports = AccountController;