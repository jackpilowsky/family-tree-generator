
export default function reducer(state={
    project: {
      name: null,
      slug: null,
      partnership: []
    },
    fetching: false,
    fetched: false,
    error: ''
  }, action) {
    switch (action.type) {
      case 'CREATE_PROJECT_START':{
        return {
          ...state
        }
      }
      case 'CREATE_PROJECT_END':{
        return {
          ...state,
          fetched: true,
          fetching: false,
          error: '',
          project: {
            name: action.payload.extras.project.name,
            slug: action.payload.extras.project.slug,
            partnership: action.payload.extras.project.partnership
          }
        }
      }
      case 'CREATE_PROJECT_FAILED':{
        return {
          ...state,
          fetched: true,
          fetching: false,
          error: action.payload.extras.msg

        }
      }
      case 'CLEAR_PROJECT_ERROR': {
        return{
          ...state,
          error: ''
        }
      }
      default: {
        return state
      }
    }
}  