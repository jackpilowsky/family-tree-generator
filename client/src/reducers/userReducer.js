
export default function reducer(state={
    userProfile: {
      email: null,
      firstName: null,
      lastName: null,
    },
    fetching: false,
    fetched: false,
    error: ''
  }, action) {
    switch (action.type) {
      case "USER_LOGOUT_START": {
        return {
          ...state,
          fetching: true
        }
      }
      case "USER_LOGGED_OUT": {
        return {
          ...state, 
          fetching: false, 
          fetched: true,
          userProfile: {
            id: null,
            firstName: null,
            lastName: null,
          }
        }
      }
      case "USER_LOGOUT_FAILED":{
        return{
          ...state, 
          fetching: false, 
          fetched: true,
          error: 'Failed to sucessfully log out user'
        }
      }
      case "USER_LOGIN_START":{
        return {
          ...state,
          fetching: true,
          fetched: false
        }
      }
      case "USER_LOGGED_IN":{
        return {
          ...state,
          fetching: false,
          fetched: true,
          userProfile: {
            email: action.payload.extras.userProfileModel.email,
            firstName: action.payload.extras.userProfileModel.firstName,
            lastName: action.payload.extras.userProfileModel.lastName
          }
        }
      }
      case "USER_LOGIN_FAILED":{
        return {
          ...state,
          error: action.payload
        }
      }
      case "USER_FETCH_START":{
        return{
          ...state
        }
      }
      case "USER_FETCHED":{
        return{
          ...state,
          fetching: false,
          fetched: true,
          userProfile: {
            email: action.payload.extras.userProfile.email,
            firstName: action.payload.extras.userProfile.firstName,
            lastName: action.payload.extras.userProfile.lastName
          }
        }
      }
      case "USER_FETCH_FAILED":{
        return{
          ...state,
          error: action.payload
        }
      }
      default:{
        return state;
      } 
    }
}
