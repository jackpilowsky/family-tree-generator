import axios from 'axios';

export function fetchUser() {
  return dispatch => {
    dispatch({type: "USER_FETCH_START"})
    axios.get("/api/fetchuser")
      .then((response) => {
        if(response.data.success){
          dispatch({type: "USER_FETCHED", payload: response.data})
        }
      })
      .catch((err) => {
        dispatch({type: "USER_FETCH_FAILED", payload: err})
      })
  };
}

export function logoutUser() {
  return dispatch => {
    dispatch({type: "USER_LOGOUT_START"})
    axios.get("/api/signout")
      .then((response) => {
        if(response.data.success){
          dispatch({type: "USER_LOGGED_OUT", payload: response.data})
        }else{
          dispatch({type: "USER_LOGOUT_FAILED", payload: response.data})
        }
      })
      .catch((err) => {
        dispatch({type: "USER_LOGOUT_FAILED", payload: err})
      })
  };
}

export function signInUser(email, password){
  return dispatch => {
    dispatch({type: "USER_LOGIN_START"})
    axios.post("/api/login", {
      email: email,
      password: password
    })
    .then((response) => {
      if(response.data.success){
        dispatch({type: "USER_LOGGED_IN", payload: response.data})
      }else{
        dispatch({type: "USER_LOGIN_FAILED", payload: response.data.extras.msg})
      }
    })
    .catch((err) => {
      dispatch({type: "USER_LOGIN_FAILED", payload: err})
    })
  };
}
export function registerUser(email, password, firstName, lastName){
  return dispatch => {
    dispatch({type: "USER_LOGIN_START"})
    axios.post("/api/signup", {
      email: email,
      password: password
    })
    .then((response) => {
      if(response.data.success){
        dispatch({type: "USER_LOGGED_IN", payload: response.data})
      }else{
        dispatch({type: "USER_LOGIN_FAILED", payload: response.data.extras.msg})
      }
    })
    .catch((err) => {
      dispatch({type: "USER_REGISTER_FAILED", payload: err})
    })
  };
}