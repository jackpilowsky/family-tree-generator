import axios from 'axios';

export function createProject(name){
  return dispatch => {
    dispatch({type: "CREATE_PROJECT_START"})
    axios.post("/api/create-project",{
      name: name
    })
    .then((response) => {
      if(response.data.success){
        dispatch({type: "CREATE_PROJECT_END", payload: response.data})
      }else{
        dispatch({type: "CREATE_PROJECT_FAILED", payload: response.data}) 
      }
    })
    .catch((err) => {
      dispatch({type: "CREATE_PROJECT_FAILED", payload: { extras: {msg: err}}})
    })
  }
}

export function clearError(){
  console.log('here')
  return dispatch => {
    console.log('here')
    dispatch({type: "CLEAR_PROJECT_ERROR"})
  }
}
