import React, { Component } from 'react';
import Header from './components/Header.jsx';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {fetchUser} from './actions/userActions';
import Body from './components/Body.jsx';
import './index.scss';





class App extends Component {
  componentDidMount(){
    this.props.fetchUser();
  }  
  render() {
    return (
      <div className="App">
        <Header />
        <Body />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user : state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    fetchUser: fetchUser
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App)
