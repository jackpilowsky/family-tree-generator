import React from 'react';
import {Col, FormGroup, ControlLabel, FormControl, Button, Alert} from 'react-bootstrap';



const SingInRegisterForm = ({self, isRegister, error, handleSubmit,  firstName, handleFirstNameChange, getFirstNameValidation, lastName, handleLastNameChange, getLastNameValidation,  email, getEmailValidation, handleEmailChange, password, getPasswordValidation, handlePasswordChange, isButtonDisabled}) => {
  var alert = '';
  if(error !== '' && error !== undefined){
    alert = (
            <Alert bsStyle="danger" onDismiss={this.handleAlertDismiss}>
              <h4>An error ocurred</h4>
              <p>Error details: {error}</p>
            </Alert>
    )
  }

  let firstAndLastName = ''
  if(isRegister){
    firstAndLastName =  (
      <div>
        <FormGroup
          controlId="firstName"
          validationState={getFirstNameValidation.call(self)}>
          <ControlLabel>First Name *</ControlLabel>
          <FormControl
            type="text"
            value={firstName}
            placeholder="Enter first name"
            onChange={handleFirstNameChange.bind(self)}
          />
          <FormControl.Feedback />
        </FormGroup>
        <FormGroup
          controlId="lastName"
          validationState={getLastNameValidation.call(self)}>
          <ControlLabel>Last Name *</ControlLabel>
          <FormControl
            type="text"
            value={lastName}
            placeholder="Enter last name"
            onChange={handleLastNameChange.bind(self)}
          />
          <FormControl.Feedback />
        </FormGroup>
      </div>
    )
  }
  return (
    <form onSubmit={handleSubmit.bind(self)}>
      <Col sm={12}>
        {alert}
        {firstAndLastName}
        <FormGroup
          controlId="emailAddress"
          validationState={getEmailValidation.call(self)}>
          <ControlLabel>Email Address *</ControlLabel>
          <FormControl
            type="text"
            value={email}
            placeholder="Enter email"
            onChange={handleEmailChange.bind(self)}
          />
          <FormControl.Feedback />
        </FormGroup>
        <FormGroup
          controlId="password"
          validationState={getPasswordValidation.call(self)}>
          <ControlLabel>Password *</ControlLabel>
          <FormControl
            type="password"
            value={password}
            placeholder="Enter password"
            onChange={handlePasswordChange.bind(self)}
          />
          <FormControl.Feedback />
        </FormGroup>
        <Button type="submit" bsStyle="primary" bsSize="large" disabled={isButtonDisabled.call(self, isRegister)}>
          Submit
        </Button>
      </Col>
    </form>
  )
}


export default SingInRegisterForm