import React from 'react';
import {Button} from 'react-bootstrap';

const HeaderButtons = ({showLogout, logoutUser}) => {
  var logoutButton = '';
  if(showLogout){
    logoutButton = (
      <Button bsStyle="primary" className="m-t" onClick={logoutUser}> 
        Log Out
      </Button>
    )
  }
  return(
    <div className="text-right">
      {logoutButton}
    </div>
  )
}

export default HeaderButtons