import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import {Navbar} from 'react-bootstrap';
import {logoutUser} from './../actions/userActions';
import HeaderButtons from './HeaderButtons.jsx'


class Header extends Component{
  render(){
    const showLogout = Boolean(this.props.user.userProfile.firstName) && Boolean(this.props.user.userProfile.lastName) && Boolean(this.props.user.userProfile.email);
    return (
      <header>
        <Navbar>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">
                <h3 className="m-a-0">Family Tree Generator</h3>
              </Link>
            </Navbar.Brand>
          </Navbar.Header>
          <HeaderButtons showLogout={showLogout} logoutUser={this.props.logoutUser} />
        </Navbar>
      </header>
    )
  }
}



const mapStateToProps = state => {
  return {
    user : state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    logoutUser: logoutUser
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Header)
