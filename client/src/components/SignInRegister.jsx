import React, { Component } from 'react';
import { Redirect } from 'react-router'
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {Col, Tabs, Tab} from 'react-bootstrap';
import SingInRegisterForm from './SingInRegisterForm.jsx';
import {signInUser, registerUser} from './../actions/userActions';


class SingInRegister extends Component{
  constructor(props){
    super(props);
    this.state = {
      email: '', 
      password: '', 
      firstName: '', 
      lastName: ''
    };
  }
  // event handlers
  handleFirstNameChange(e){
    this.setState({firstName: e.target.value})
  }
  handleLastNameChange(e){
    this.setState({lastName: e.target.value})
  } 
  handleEmailChange(e){
    this.setState({ email: e.target.value });
  }
  handlePasswordChange(e){
    this.setState({password: e.target.value});
  }
  // validators
  getFirstNameValidation(){
    return (this.state.firstName.length > 0 ? 'success' : null)
  }
  getLastNameValidation(){
    return (this.state.lastName.length > 0 ? 'success' : null)
  }
  getEmailValidation(){
    let returnValue = 'error';
    if(!this.state.email || this.state.email.length === 0){
      returnValue = null;
    }
    if (/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(this.state.email)){
      returnValue = 'success';
    }
    return returnValue;
  }
  getPasswordValidation(){
    const length = this.state.password.length;
    if(length === 0)return null;
    if(length < 6) return 'error';
    return 'success';
  }
  isButtonDisabled(isRegister){
    if(isRegister){
      return !this.getFirstNameValidation() || !this.getLastNameValidation() || this.getEmailValidation() !== 'success' || this.getPasswordValidation() !== 'success';
    }else{
      return this.getEmailValidation() !== 'success' || this.getPasswordValidation() !== 'success'
    }
  }
  handleSignIn(e){
    e.preventDefault();
    this.props.signInUser(this.state.email, this.state.password);
  }  
  handleRegister(e){
    e.preventDefault();
    this.props.registerUser(this.state.email, this.state.password, this.state.firstName, this.state.lastName);
  }
  render(){
    return(
      <Col sm={6} smOffset={3} > 
        <Tabs defaultActiveKey={1} id="uncontrolled-tab-example">
          <Tab eventKey={1} title="Sign In" className="m-t">
            <SingInRegisterForm 
              self={this}
              isRegister={false} 
              error={this.props.user.error} 
              handleSubmit={this.handleSignIn.bind(this)}  
              firstName={this.firstName}
              handleFirstNameChange={this.handleFirstNameChange}
              getFirstNameValidation={this.getFirstNameValidation}
              lastName={this.props.lastName}
              handleLastNameChange={this.handleLastNameChange}
              getLastNameValidation={this.getLastNameValidation}
              email={this.props.email}
              getEmailValidation={this.getEmailValidation}
              handleEmailChange={this.handleEmailChange}
              password={this.props.password}
              getPasswordValidation={this.getPasswordValidation}
              handlePasswordChange={this.handlePasswordChange} 
              isButtonDisabled={this.isButtonDisabled} />
          </Tab>
          <Tab eventKey={2} title="Register" className="m-t">
            <SingInRegisterForm 
              self={this}
              isRegister={true} 
              alert={alert} 
              handleSubmit={registerUser}  
              firstName={this.firstName}
              handleFirstNameChange={this.handleFirstNameChange}
              getFirstNameValidation={this.getFirstNameValidation}
              lastName={this.lastName}
              handleLastNameChange={this.handleLastNameChange}
              getLastNameValidation={this.getLastNameValidation}
              email={this.email}
              getEmailValidation={this.getEmailValidation}
              handleEmailChange={this.handleEmailChange}
              password={this.password}
              getPasswordValidation={this.getPasswordValidation}
              handlePasswordChange={this.handlePasswordChange} 
              isButtonDisabled={this.isButtonDisabled} />
          </Tab>
        </Tabs>
      </Col>
    )
  }
}

const mapStateToProps = state => {
  return {
    user : state.user
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    signInUser: signInUser,
    registerUser: registerUser
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SingInRegister)