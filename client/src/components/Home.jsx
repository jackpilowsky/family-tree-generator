import React, { Component } from 'react';
import {Col} from 'react-bootstrap';
import { Link} from 'react-router-dom'

class Home extends Component{
  constructor(props){
    super(props);
    this.state ={
      height: 0
    }
  }
  componentDidMount(){
    const windowHeight = document.getElementsByTagName('html')[0].clientHeight;
    const headerHeight = document.getElementsByTagName('header')[0].clientHeight;
    this.setState({ height: windowHeight - headerHeight});
  }
  render(){
    const containerStyles = {height: this.state.height};
    return(
      <div className="container" style={containerStyles}>
        <Col sm={12} className="text-center full-height m-t-lg">
          <Link className="btn btn-lg btn-success" to="/create-project">
            Create New Project
          </Link>
        </Col>
      </div>
    )
  }
}
export default Home