import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {createProject, clearError} from './../actions/projectActions';
import {Alert, Panel, Col, FormGroup, ControlLabel, FormControl, Button, HelpBlock} from 'react-bootstrap'


class CreateProject extends Component{
  constructor(props){
    super(props);
    this.state = {
      name: ''
    }
  }
  componentDidMount(){
    console.log(this.props)
  }
  handleNameChange(e){
    this.setState({name: e.target.value})
  }
  handleSubmit(e){
    this.props.createProject(this.state.name);
    e.preventDefault();
  } 
  validateName(){
    if(this.state.name.length === 0){
      return null;
    }
    if(this.state.name.length > 3){
      return 'success'
    }
    return 'error';
  }
  isButtonDisabled(){
    return this.validateName() !== 'success'
  }
  handleAlertDismiss(){
    this.props.error = '';
  }  
  render(){
    var alert = '';
    if(this.props.project.error !== '' && this.props.project.error !== undefined){
      alert = (
              <Alert bsStyle="danger" onDismiss={clearError}>
                <h4>An error ocurred</h4>
                <p>Error details: {this.props.project.error}</p>
              </Alert>
      )
    }
    return(
      <Col sm={6} smOffset={3}>
        <Panel header={'Create Project'}>
          {alert}
          <form onSubmit={this.handleSubmit.bind(this)}>
            <Col sm={12}>
              <FormGroup
                controlId="name"
                validationState={this.validateName()}>
                <ControlLabel>Project Name *</ControlLabel>
                <FormControl
                  type="text"
                  value={this.state.name}
                  placeholder="Enter project name"
                  onChange={this.handleNameChange.bind(this)}
                />
                <HelpBlock>Name must be at least 4 characters</HelpBlock>
                <FormControl.Feedback />
              </FormGroup>
              <Button bsStyle="primary" bsSize="large" type="submit" disabled={this.isButtonDisabled()}>
                Submit
              </Button>
            </Col>
          </form>
        </Panel>
      </Col>
    )
  }
}

const mapStateToProps = state => {
  return {
    user : state.user,
    project: state.project
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    createProject: createProject,
    clearError: clearError
  }, dispatch)
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateProject)