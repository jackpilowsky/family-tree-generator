import React, { Component } from 'react';
import { Route} from 'react-router-dom'
import SignInRegister from './SignInRegister.jsx';
import CreateProject from './CreateProject.jsx';
import Home from './Home.jsx';

class Body extends Component{
  render(){
    return(
      <main className="full-height">
        <div className="container">
          <Route exact path="/" component={Home} />
          <Route exact path="/signin" component={SignInRegister} />
          <Route exact path="/create-project" component={CreateProject} />

        </div>
      </main>
    )
  }
}
export default Body