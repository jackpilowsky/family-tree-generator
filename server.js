const express = require("express");
const app = express();

//set up body partser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// db connection
const dbConfig = require('./db/db.js');
const mongoose = require('mongoose');
mongoose.connect(dbConfig.url, { useMongoClient: true });

//set up session
const expressSession = require('express-session');
app.use(expressSession({
  secret: 'familyTreeSecret',
  resave: true,
  saveUninitialized: true
}));

// set routes
const userRoutes = require('./lib/routes/users.js')(expressSession, app);
app.use(userRoutes);
const projectRoutes = require('./lib/routes/projects.js')(expressSession, app);
app.use(projectRoutes);



const isValidPassword = function(user, password){
  return bCrypt.compareSync(password, user.password);
}
const createHash = function(password){
 return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}



app.set("port", process.env.PORT || 3001);

// Express only serves static assets in production
if (process.env.NODE_ENV === "production") {
  app.use(express.static("client/build"));
}



app.listen(app.get("port"), () => {
  console.log(`Find the server at: http://localhost:${app.get("port")}/`); // eslint-disable-line no-console
});